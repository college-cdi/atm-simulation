<a name="readme-top"></a>

[![Contributors][contributors-shield]][contributors-url]
[![Forks][forks-shield]][forks-url]
[![Stargazers][stars-shield]][stars-url]
[![MIT License][license-shield]][license-url]
[![LinkedIn][linkedin-shield]][linkedin-url]

<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://gitlab.com/college-cdi/atm-simulation">
    <img src="docs/images/atm-simulationx-high-resolution-logo-transparent.png" height="200px" alt="Logo" >
  </a>

  <h3 align="center">atm-simulation</h3>
    Simple CLI app simulating atm.
  <p align="center">
    <br />
    <a href="#"><strong>Explore documentation »</strong></a>
    <br />
    <br />
    <a href="https://gitlab.com/college-cdi/atm-simulation/issues">Report Bug</a>
    ·
    <a href="https://gitlab.com/college-cdi/atm-simulation/issues">Request Feature</a>
  </p>
</div>



<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#contribute">Contribute</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
    <li><a href="#ressources">Ressources</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About the project

[![Product Name Screen Shot][product-screenshot]](docs/images/screenshot.png)


  Simple cli app that simulates an atm.

<!-- There are many great README templates available on gitlab; however, I didn't find one that really suited my needs so I created this enhanced one. I want to create a README template so amazing that it'll be the last one you ever need -- I think this is it.

Here's why:
* Your time should be focused on creating something amazing. A project that solves a problem and helps others
* You shouldn't be doing the same tasks over and over like creating a README from scratch
* You should implement DRY principles to the rest of your life :smile:

Of course, no one template will serve all projects since your needs may be different. So I'll be adding more in the near future. You may also suggest changes by forking this repo and creating a pull request or opening an issue. Thanks to all the people have contributed to expanding this template! -->

<p align="right">(<a href="#readme-top">back to top</a>)</p>


<!-- BUILT WITH -->
### Built with

* [![dotnet-shield]][dotnet-url]
* [![Postgres-shield]][Postgres-url]

<p align="right">(<a href="#readme-top">back to top</a>)</p>



<!-- GETTING STARTED -->
## Getting started

### Prerequisites

- .Net Core 8
- Postgres
  

### Installation

1. Rename the file ``.template.env`` to ``.env``
2. Put your credentials 
<!-- ```yml
PORT=
MONGODB_HOSTNAME=
MONGODB_PORT=
MONGODB_DATABASE=
``` -->
3. Install dependencies : 
```sh
dotnet restore
```
3. Run the app
```sh
dotnet start
```

<p align="right">(<a href="#readme-top">back to top</a>)</p>


## Usage

Handles account creation, credit and transactions.

<p align="right">(<a href="#readme-top">back to top</a>)</p>


<!-- CONTRIBUTE -->
## Contribute

1. Clone the repo `git clone `
2. Create a new "Feature" branch (`git checkout -b feature/{FeatureName}`)
3. Commit changes (`git commit -m 'x problem solved'`)
4. Push to your own branch (`git push origin feature/{FeatureName}`)
5. Create a Pull Request 

<p align="right">(<a href="#readme-top">back to top</a>)</p>


<!-- LICENSE -->
## License
Distributed under the MIT License. See LICENSE.txt for more information.

<p align="right">(<a href="#readme-top">back to top</a>)</p>



<!-- CONTACT -->
## Contact

Samuel Jubinville-Baril - [gitlab](https://gitlab.com/samueljubinville119) - samuel.jubinville@outlook.com

Project Link: [https://gitlab.com/college-cdi/atm-simulation](https://gitlab.com/college-cdi/atm-simulation)

<p align="right">(<a href="#readme-top">back to top</a>)</p>



<!-- Ressources -->
## Ressources

* [README Template](https://gitlab.com/othneildrew/Best-README-Template)


<p align="right">(<a href="#readme-top">back to top</a>)</p>



<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
<!-- gitlab URLS -->
<!-- Custom Badge Link https://shields.io/badges -->
[contributors-shield]: https://img.shields.io/gitlab/contributors/college-cdi/atm-simulation.svg?style=for-the-badge
[contributors-url]: https://gitlab.com/college-cdi/atm-simulation/graphs/contributors
[forks-shield]: https://img.shields.io/gitlab/forks/college-cdi/atm-simulation.svg?style=for-the-badge
[forks-url]: https://gitlab.com/college-cdi/atm-simulation/network/members
[stars-shield]: https://img.shields.io/gitlab/stars/college-cdi/atm-simulation.svg?style=for-the-badge
[stars-url]: https://gitlab.com/college-cdi/atm-simulation/stargazers
[issues-shield]: https://img.shields.io/gitlab/issues/college-cdi/atm-simulation.svg?style=for-the-badge
[issues-url]: https://gitlab.com/college-cdi/atm-simulation/issues
[license-shield]: https://img.shields.io/gitlab/license/college-cdi%2Fatm-simulation?style=for-the-badge
[license-url]: https://gitlab.com/college-cdi/atm-simulation/blob/master/LICENSE.txt
[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=for-the-badge&logo=linkedin&colorB=555
[linkedin-url]: https://www.linkedin.com/in/samuel-jubinville-baril-bbb5601a4/
[product-name]: atm
[product-screenshot]: docs/images/screenshot.png
[Product-name-screenshot]: Oauth-api
[product-menu-screenshot]: docs/images/Game_menu_screenshot.png
[form-screenshot]: docs/images/form.png
[Form Screen Shot]: Form
[DynamoDB Name Screen Shot]: DynamoDB-screenshot
[dynamodb-screenshot]: docs/images/dynamodb.png



<!-- FRAMEWORK AND LIBRARY URLS -->
[dotnet-shield]: https://img.shields.io/badge/.NET-5C2D91?style=for-the-badge&logo=.net&logoColor=white
[dotnet-url]: https://dotnet.microsoft.com/
[React-shield]: https://img.shields.io/badge/React-20232A?style=for-the-badge&logo=react&logoColor=61DAFB
[React-url]: https://reactjs.org/
[Node-shield]: https://img.shields.io/badge/node.js-6DA55F?style=for-the-badge&logo=node.js&logoColor=white
[Node-url]: https://nodejs.org/
[Python-shield]: https://img.shields.io/pypi/pyversions/FastAPI?logo=python
[Python-url]: (https://www.python.org/)
[ExpressJs-shield]: https://img.shields.io/badge/express.js-%23404d59.svg?style=for-the-badge&logo=express&logoColor=%2361DAFB/
[ExpressJs-url]: https://expressjs.com/
[FastAPI-shield]: https://img.shields.io/badge/FastAPI-009688?style=for-the-badge&logo=FastAPI&logoColor=white
[FastAPI-url]: https://fastapi.tiangolo.com/
[Vue-shield]: https://img.shields.io/badge/Vue.js-35495E?style=for-the-badge&logo=vuedotjs&logoColor=4FC08D
[Vue-url]: https://vuejs.org/
[Kotlin-shield]: https://img.shields.io/badge/kotlin-%237F52FF.svg?style=for-the-badge&logo=kotlin&logoColor=white
[Kotlin-url]: https://kotlinlang.org/
[Postgres-shield]: https://img.shields.io/badge/postgres-%23316192.svg?style=for-the-badge&logo=postgresql&logoColor=white
[Postgres-url]: https://www.postgresql.org/
[MongoDB-shield]: https://img.shields.io/badge/MongoDB-%234ea94b.svg?style=for-the-badge&logo=mongodb&logoColor=white
[MongoDB-url]: https://www.mongodb.com/
[Gradle-shield]: https://img.shields.io/badge/Gradle-02303A.svg?style=for-the-badge&logo=Gradle&logoColor=white
[Gradle-url]: https://gradle.org/
[Android-shield]: https://img.shields.io/badge/Android-3DDC84?style=for-the-badge&logo=android&logoColor=white
[Android-url]: https://developer.android.com/
[Docker-shield]: https://img.shields.io/badge/docker-%230db7ed.svg?style=for-the-badge&logo=docker&logoColor=white
[Docker-url]: https://www.docker.com/