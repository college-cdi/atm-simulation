using atm.Data;

namespace atm.Tests.Classes;

public class Client : atm.Classes.Client
{
    public Client(ApplicationDbContext context) : base(context)
    {
    }

    public Client(ApplicationDbContext context, string email, string nip) : base(context) {
        this.Email = email;
        foreach (char c in nip) {
            this.Nip.AppendChar(c);
        }
    }
    
}