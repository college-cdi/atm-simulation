using Microsoft.EntityFrameworkCore;
using atm.Models;
using Microsoft.Extensions.DependencyInjection;

namespace atm.Data;

public class TestApplicationDbContext : ApplicationDbContext {

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseInMemoryDatabase("Database"); // Forces to use InMemoryDB for tests.
    }


    public void Seed(List<User> users, List<Models.Account> accounts, List<Models.Transaction> transactions) {
        foreach (User user in users) {
            user.Id = new Guid();
            user.Nip = Authenticate.Authenticate.Encrypt(user.Nip, out byte[] saltKey)!;
            user.Hash = saltKey;
        }
        this.Users!.AddRange(users);
        this.Accounts!.AddRange(accounts);
        this.Transactions!.AddRange(transactions);
        this.SaveChanges();
    }

}
