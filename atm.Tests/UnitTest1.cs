
using System.Net.Security;
using atm.Classes;
using atm.Data;
using atm.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualBasic;
using Npgsql.Replication;
using Sharprompt;
using Xunit.Sdk;

namespace atm.Tests;

public class UnitTest1
{
    [Fact]
    public void Test1()
    {
        var TestEmail = "Test@email.com";
        var TestNip = "1234";
        var TestName = "Super";
        var _context = Initialize();

        var client = new atm.Tests.Classes.Client(_context, TestEmail, TestNip);
        client.SetFirstName(TestName);
        client.Login();

        Assert.Equal(TestName, client.FirstName);
        Assert.NotNull(client.Id);
        Assert.IsType<Guid>(client.Id);
    }

    private static TestApplicationDbContext Initialize() {
        var _context = new TestApplicationDbContext();
        Authenticate.Authenticate.New(_context);
        var users = new List<User> {
            new() { FirstName = "Super", LastName = "User", Nip = "1234", Email = "Test@email.com"},
        };

        _context.Seed(users, new List<atm.Models.Account>(), new List<Transaction>());
        return _context;
    }
}