using System;
using System.Reflection;
using System.Runtime.InteropServices.Marshalling;
using atm.Controllers;
using atm.Data;
using atm.Models;
using atm.Models.Form;
using Microsoft.EntityFrameworkCore;
using System.Text.RegularExpressions;

namespace atm.Classes;

/// <Summary>
/// Classes Wrapper pour <c>Sharprompt</c>.
/// </Summary>
public sealed class Application {
    private static Application? _instance;
    private readonly ApplicationDbContext _context;
    private readonly ClientController _clientController;
    private readonly AccountController _accountController;
    private readonly TransactionController _transactionController;
    private readonly BankController _bankController;
    private Bank _bank;
    private User _client;
    private Boolean stop;

    // Constructeur
    private Application(ApplicationDbContext context) {
        if (context is null) throw new Exception();
        this._context = context;
        this._client = new atm.Models.User();
        this._clientController = new ClientController(context);
        this._accountController = new AccountController(context);
        this._transactionController = new TransactionController(context);
        this._bankController = new BankController(context);
        this._bank = this._bankController.Get();
    }


    public static Application New(ApplicationDbContext context) {
        if (Application._instance is not null) {
            return Application._instance;
        }
        Application._instance = new Application(context);
        return Application._instance;
    }


    public static Application GetInstance() {
        if (_instance is null) {
            throw new Exception();
        }
        return _instance;
    }


    public void Run() {
        while (!this.stop) {
            Console.WriteLine("Veuillez vous connecter pour procédé.");
            this.LogUser();
            if (this._client.Role == Roles.Admin) {
                this.ServeAdminActions();
            } else {
                this.ServeUserActions();
            }
        }
            
    }


    private Boolean LogUser() {
        while (!this._client.isLoggedIn) {
            try {
                var loginForm = new LoginForm();
                loginForm.Email = Sharprompt.Prompt.Input<string>("Veuillez entrer votre courriel.");
                loginForm.Nip = Sharprompt.Prompt.Password("Veuillez entrer votre NIP.");
                this._client = this._clientController.Login(loginForm);
            } catch (Exceptions.LoginFailedException ex) {
                this.WriteErrorMessage(ex);
            } catch (Exception ex) {
                this.WriteErrorMessage(ex);
            }
        }
        return this._client.isLoggedIn;
    }


#region "Serve Menus Actions"

    private void ServeUserActions() {
        while (this._client.isLoggedIn) {
            var input = Sharprompt.Prompt.Select<UserActionMenu>("Veuillez choisir l'action que vous souhaitez faire");
            switch(input) {
                case UserActionMenu.Deposit:
                    try {
                        this.Deposit();
                    } catch (Exception e) {
                        this.WriteErrorMessage(e);
                    }
                    break;
                case UserActionMenu.Withdrawal:
                    try {
                        this.Whitdraw();
                    } catch (Exception e) {
                        this.WriteErrorMessage(e);
                    }
                    break;
                case UserActionMenu.Transfert:
                    try {
                        this.Transfert();
                    } catch (Exception e) {
                        this.WriteErrorMessage(e);
                    }
                    break;
                case UserActionMenu.BillPayment:
                    try {
                        this.PayBills();
                    } catch (Exception e) {
                        this.WriteErrorMessage(e);
                    }
                    break;
                case UserActionMenu.ConsultAccounts:
                    this.ConsultAccounts();
                    break;
                case UserActionMenu.Logout:
                    this._clientController.Logout(this._client);
                    break;
                default:
                    Console.WriteLine("Veuillez choisir une option valide.");
                    break;
            }
        }
    }


    private void ServeAdminActions() {
        while (this._client.isLoggedIn) {
            var input = Sharprompt.Prompt.Select<AdminActionMenu>("Veuillez choisir une action");
            switch(input) {
                case AdminActionMenu.CreateClient:
                    try {
                        this.CreateClient();
                    } catch (Exception ex) {
                        this.WriteErrorMessage(ex);
                    }
                    break;
                case AdminActionMenu.CreateAccount: 
                    try {
                        this.CreateAccount();
                    } catch (Exception ex) {
                        this.WriteErrorMessage(ex);
                    }
                    break;
                case AdminActionMenu.DisplayAccountTransaction:
                    try {
                        this.DisplayAccountTransaction();
                    } catch(Exception ex) {
                        this.WriteErrorMessage(ex);
                    }
                    break;
                case AdminActionMenu.ToggleLockClientAccount:
                    this.ToggleLockClient();
                    break;
                case AdminActionMenu.AddMoneyToATM: 
                    try {
                        this.AddFunds();
                    } catch (Exception ex) {
                        this.WriteErrorMessage(ex);
                    }
                    break;
                case AdminActionMenu.StopApp:
                    this.CloseAtm();
                    break;
                case AdminActionMenu.PayInterest:
                    try {
                        this.PaySavingInterest();
                    } catch (Exception ex) {
                        this.WriteErrorMessage(ex);
                    }
                    // Saving accounts
                    break;
                case AdminActionMenu.CollectMortgage:
                    try {
                        this.CollectMortgage();
                    } catch (Exception ex) {
                        this.WriteErrorMessage(ex);
                    }
                    break;
                case AdminActionMenu.CollectCreditInterest:
                    try {
                        this.CollectInterest();
                    } catch (Exception ex) {
                        this.WriteErrorMessage(ex);
                    }
                    break;
                case AdminActionMenu.Logout:
                default:
                    this._clientController.Logout(this._client);
                    break;
            }
        }
    }

#endregion


#region "UserMenuActions"

    private void Deposit() {
        if (this._client!.Accounts?.Count <= 0) {
            throw new Exception("L'utilisateur n'a aucun compte actif.");
        }
        var depositAccountList = this._client.Accounts!.Where(x => x.Type != Account_Type.CREDIT).ToList();
        if (depositAccountList.Count <= 0 ) throw new Exception("Aucun compte n'a été trouvé");
        var currentAccount = this.GetAccount(depositAccountList);
        var amount = Math.Round(Sharprompt.Prompt.Input<double>($"Combien voulez-vous déposer dans le compte ?"), 2);

        switch(currentAccount.Type){
            case Account_Type.CREDIT:
                this.WriteErrorMessage(new Exception("Impossible de déposer directement dans un compte Crédit ou hypothécaire. Veuillez payer une facture."));
                break;
            case Account_Type.CHEQUING:
            case Account_Type.SAVING:
            case Account_Type.MORTGAGE:
            default:
                currentAccount.Amount += amount;
                this._accountController.Update(currentAccount);
                var transaction = this.CreateTransaction(currentAccount, amount, Transaction_Type.DEPOSIT);
                this.WriteTransactionSuccessMessage(currentAccount, transaction);
                break;
        }

    }


    private void Whitdraw() {
        if (this._client!.Accounts?.Count <= 0) {
            throw new Exception("L'utilisateur n'a aucun compte actif.");
        }
        var whitdrawalAccountList = this._client.Accounts!.Where(x => x.Type == Account_Type.CHEQUING || x.Type == Account_Type.SAVING).ToList();
        var selectedAccount = this.GetAccount(whitdrawalAccountList);
        double totalAmount = this.GetAmount(selectedAccount);
        double amount = 0;

        switch(selectedAccount.Type) {
            case Account_Type.MORTGAGE:
            case (Account_Type.CREDIT):
                throw new Exception("Impossible de retiré d'un compte hypothécaire ou crédit");               
            case (Account_Type.CHEQUING):
            case (Account_Type.SAVING):
            default:
                amount = this.CalculateCreditWithdrawal(totalAmount, selectedAccount);
                selectedAccount.Amount -= amount;
                break;
        }
        this._bank.Amount -= totalAmount;
        this._accountController.Update(selectedAccount);
        this._bankController.Update(this._bank);
        var transaction = this.CreateTransaction(selectedAccount, amount, Transaction_Type.WHITDRAWAL);
        this.WriteTransactionSuccessMessage(selectedAccount, transaction);
    }


    private void Transfert() {
        if (this._client!.Accounts?.Count <= 1) {
            throw new Exception("Transfert impossible. Il faut au minimum 2 compte actif pour effectuer un transfert");
        }
        var chequingAccountList = this._client.Accounts!.Where(x => x.Type == Account_Type.CHEQUING).ToList();
        if (chequingAccountList.Count <= 0) {
            throw new Exception("Opération impossible. Aucun compte chèque n'a été trouvé.");
        }
        var sourceAccount = this.GetAccount(chequingAccountList);
        var destinationAccount = this.GetAccount(this._client!.Accounts!.Where(x => x.Id != sourceAccount.Id).ToList());
        double amount = Math.Round(Sharprompt.Prompt.Input<double>($"Combien voulez-vous transférer ?"), 2);
        
        if (sourceAccount.Amount < amount && destinationAccount.Type != Account_Type.CREDIT ) {
            amount = this.CalculateCreditWithdrawal(amount, sourceAccount, Transaction_Type.TRANSFER);
        } else if (sourceAccount.Amount < amount) {
            throw new Exception("Opération impossible: Le solde du compte est inférieur au montant de la transaction.");
        }

        switch(sourceAccount.Type) {
            case (Account_Type.SAVING):
            case (Account_Type.CREDIT):
            case Account_Type.MORTGAGE:
                throw new Exception("Opération impossible: Vous ne pouvez pas transférer à partir d'un compte hypothécaire, crédit ou épargne.");               
            case (Account_Type.CHEQUING):
            default:
                sourceAccount.Amount -= amount;
                break;
        }

        switch(destinationAccount.Type) {
            case (Account_Type.CREDIT):
                destinationAccount.Amount -= amount;
                break;
            case (Account_Type.CHEQUING):
            case (Account_Type.SAVING):
            case Account_Type.MORTGAGE:
            default:
                destinationAccount.Amount += amount;
                break;
        }

        this._accountController.Update(sourceAccount);
        this._accountController.Update(destinationAccount);
        var transaction = this.CreateTransaction(sourceAccount, amount, Transaction_Type.TRANSFER, destinationAccount);
        this.WriteTransactionSuccessMessage(sourceAccount, transaction);
    }


    private void PayBills() {
        if (this._client!.Accounts?.Count <= 1) {
            throw new Exception("Transfert impossible. Il faut au minimum 2 compte actif pour effectuer un transfert");
        }
        var chequingAccountList = this._client.Accounts!.Where(x => x.Type == Account_Type.CHEQUING).ToList();
        var creditAccount = this._client.Accounts!.Where(x => x.Type == Account_Type.CREDIT).FirstOrDefault();
        if (chequingAccountList.Count <= 0) {
            throw new Exception("Opération impossible. Aucun compte chèque n'a été trouvé.");
        }
        var sourceAccount = this.GetAccount(chequingAccountList);
        var billAccount = Sharprompt.Prompt.Input<string>("Quel est le nom du service facturé?");
        var amount = Math.Round(Sharprompt.Prompt.Input<double>($"Combien voulez-vous déposer dans le compte ?"), 2);
        const double BILL_FEES = 1.25;
        
        if (sourceAccount.Amount < (amount + BILL_FEES)) {
            // TODO: Implement credit automatic whitdraw?
            throw new Exception("Opération impossible: Le solde du compte est inférieur au montant de la transaction.");
        }

        sourceAccount.Amount -= (amount + BILL_FEES);
        this._accountController.Update(sourceAccount);
        var transaction = this.CreateTransaction(sourceAccount, amount + BILL_FEES, Transaction_Type.BILLING, null, billAccount);
        this.WriteTransactionSuccessMessage(sourceAccount, transaction);
    }


    private void ConsultAccounts() {
        if (this._client!.Accounts?.Count <= 0) {
            throw new Exception("L'utilisateur n'a aucun compte actif.");
        }
        var message = this.FormatAccountHeader();
        Console.WriteLine(message);
        foreach (Account account in this._client!.Accounts!) {
            switch(account.Type) {
                case (Account_Type.CREDIT):
                    Console.ForegroundColor = account.Amount < account.Limit ? ConsoleColor.Green : ConsoleColor.Red;
                    break;               
                case (Account_Type.CHEQUING):
                case (Account_Type.SAVING):
                case Account_Type.MORTGAGE:
                default:
                    Console.ForegroundColor = account.Amount < 0 ? ConsoleColor.Red : ConsoleColor.Green;
                    break;
            }
            Console.WriteLine(this.FormatAccount(account));
            Console.ResetColor();
        }
        Console.Write("Appuyer sur une touche pour continuer");
        Console.ReadLine();
    }

#endregion


#region "AdminMenuActions"

    private void CreateClient() {
        string email = "";
        string nip = "";
        string validationNip = "";
        string phoneNumber = "";
        int tries = 0;

        var firstName = Sharprompt.Prompt.Input<string>("Quel est le prénom du nouveau client?");
        var lastName = Sharprompt.Prompt.Input<string>("Quel est le nom de famille du nouveau client?");

        while (!Regex.IsMatch(email, @"^[^@\s]+@[^@\s]+\.[^@\s]+$",RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250))) {
            if (tries > 0) {
                this.WriteErrorMessage("La valeur entrée n'est pas un email valide.");
            }
            email = Sharprompt.Prompt.Input<string>("Quel est le email du client?");
            tries++;
        }
        tries = 0;

        while (phoneNumber.Length != 10) {
            if (tries > 0) {
                this.WriteErrorMessage("Vous devez entrer un numéro de téléphone valide. Le formet est [ 1234567890 ]");
            }
            phoneNumber = Sharprompt.Prompt.Input<string>("Quel est le numéro de téléphone du client?");
            tries++;
        }
        tries = 0;

        while (nip != validationNip || !Regex.IsMatch(nip, @"^[0-9]{4,6}$", RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250))) {
            if (tries > 0) {
                if (nip != validationNip) {
                    this.WriteErrorMessage("Le mot de passe et le mot de passe de confirmation ne sont pas identique.");
                } else if (nip.Length < 4 || nip.Length > 6) {
                    this.WriteErrorMessage("Le mot de passe doit contenir entre 4 et 6 numéro.");
                }
            }
            nip = Sharprompt.Prompt.Password("Veuillez entrer le nouveau nip");
            validationNip = Sharprompt.Prompt.Password("Veuillez confirmer le nip");
            tries++;
        }

       var role = Sharprompt.Prompt.Select<Roles>("Quel rôle l'utilisateur aura?");

        var user = new User{FirstName = firstName, LastName = lastName, PhoneNumber = "1234567890", Nip = nip, Email = email, Role = role };
        user = this._clientController.Insert(user);
        if (user.Role != Roles.Admin) {
            this._accountController.Insert(new Account {
                UserId = user.Id,
                Type = Account_Type.CHEQUING});
        }

        this.WriteCreateSuccessMessage("Utilisateur");
        Console.WriteLine(user.Id);
    }


    private void CreateAccount() {
        var users = this._clientController.GetAll();
        var selectedUser = this.GetUser(users);
        Console.WriteLine(selectedUser.Id);
        var accountType = Sharprompt.Prompt.Select<Account_Type>("Quel type de compte voulez-vous créer?");

        var account = new Account { UserId = selectedUser.Id, Type = accountType};
        this._accountController.Insert(account);
    }


    private void DisplayAccountTransaction() {
        var transactions = this._transactionController.GetAll();
        if (transactions.Count <= 0) {
            throw new Exception("Aucune transaction existes pour le moment.");
        }
        bool chooseAccount = Sharprompt.Prompt.Confirm("Souhaitez-vous chercher un compte à partir du client?");
        if (chooseAccount) {
            var users = this._clientController.GetAll();
            var selectedUser = this.GetUser(users);
        }

        Console.WriteLine(this.FormatTransactionHeader());
        foreach(Transaction transaction in transactions) {
            Console.WriteLine(this.FormatTransaction(transaction));
        }
        Console.Write("Appuyer sur une touche pour continuer...");
        Console.ReadLine();
    }


    private void ToggleLockClient() {
        var users = this._clientController.GetAll();
        var selectedUser = this.GetUser(users);
        selectedUser.IsLocked = !selectedUser.IsLocked;
        this._clientController.Update(selectedUser);
        var action = selectedUser.IsLocked ? "bloqué" : "débloqué";
        this.WriteCreateSuccessMessage($"L'utilisateur a été {action} avec succès!");
        Console.Write("Appuyer sur une touche pour continuer...");
        Console.ReadLine();
    }


    private void AddFunds() {
        double amount = Math.Round(Sharprompt.Prompt.Input<double>("Combien voulez-vous déposé dans la banque?"), 2);
        if ((this._bank.Amount + amount) > 20000) {
            throw new Exception("Pour des mesures de sécurités, nous ne pouvons accépter un dépôt qui augmenterais le solde de plus de 20000$");
        }
        this._bank.Amount += amount;
        this._bankController.Update(this._bank);
        this.WriteCreateSuccessMessage($"Le montant de {amount}$ a été ajouté avec succès");
        Console.Write("Appuyer sur une touche pour continuer...");
        Console.ReadLine();
    }


    private void PaySavingInterest() {
        var accounts = this._accountController.GetAccountsBy(Account_Type.SAVING);
        if (accounts is null) {
            throw new Exception("Aucun compte n'a été trouvé");
        }
        Console.WriteLine(this.FormatAccountInterestHeader());
        foreach (Account account in accounts) {
            var fee = Math.Round(account.Amount * (account.Interest / 100));
            this.DisplayAccountInterestAction(account, fee, account.Type);
            account.Amount += fee;
            this.CreateTransaction(account, fee, Transaction_Type.INTEREST);
        }
        Console.Write("\nAppyer sur une touche pour continuer...");
        Console.ReadLine();
    }


    private void CollectInterest() {
        var accounts = this._accountController.GetAccountsBy(Account_Type.CREDIT);
        if (accounts is null) {
            throw new Exception("Aucun compte n'a été trouvé");
        }
        Console.WriteLine(this.FormatAccountInterestHeader());
        foreach (Account account in accounts) {
            var fee = Math.Round(account.Amount * (account.Interest / 100));
            this.DisplayAccountInterestAction(account, fee, account.Type);
            account.Amount -= fee;
            this.CreateTransaction(account, fee, Transaction_Type.INTEREST);
        }       
        Console.Write("\nAppyer sur une touche pour continuer...");
        Console.ReadLine();
    }


    private void CollectMortgage() {
        var accounts = this._accountController.GetAccountsBy(Account_Type.MORTGAGE);
        if (accounts is null) {
            throw new Exception("Aucun compte n'a été trouvé");
        }
        var selectedAccount = this.GetMortgageAccount(accounts);
        Console.WriteLine($"Compte sélectionné: {this.FormatMortgageAccount(selectedAccount)}");
        var amount = Math.Round(Sharprompt.Prompt.Input<double>("Combien souhaitez-vous collecté?"), 2);
        if (amount > selectedAccount.Amount) {
            amount = this.CalculateCreditWithdrawal(amount, selectedAccount, Transaction_Type.INTEREST);
        }
        selectedAccount.Amount -= amount;
        this._accountController.Update(selectedAccount);
        var transaction = this.CreateTransaction(selectedAccount, amount, Transaction_Type.INTEREST);
        this.WriteTransactionSuccessMessage(selectedAccount, transaction);
    }


    private void CloseAtm() {
        this.stop = true;
        this._clientController.Logout(this._client);
        this.WriteWarning("Fermeture du guichet...\nAppuyez sur une touche pour continuer.");
        Console.ReadLine();
    }

#endregion


#region "Helpers function"

    private Account GetSelectedAccount(string selectedAccount, List<Account> accounts) {
        var id = Guid.Parse(selectedAccount.Substring(1, 37));
        Account? account = accounts.Where(x => x.Id == id).FirstOrDefault();
        if (account is null) {
            throw new Exception($"Erreur lors de la tentative de récupération du compte #{id}");
        }
        return account;
    }


    private User GetSelectedUser(string selectedUser) {
        var id = Guid.Parse(selectedUser.Substring(1, 37));
        User? user = this._clientController.GetBy(id);
        if (user is null) {
            throw new Exception($"Erreur lors de la tentative de récupération de l'utilisateur #{id}");
        }
        return user;
    }


    private void WriteWarning(string message) {
        Console.ForegroundColor = ConsoleColor.Yellow;
        Console.WriteLine(message);
        Console.ResetColor();
    }


    private void WriteErrorMessage(Exception e) {
        Console.ForegroundColor = ConsoleColor.Red;
        Console.WriteLine(e.Message);
        Console.ResetColor();
        Console.Write("Appuyer sur une touche pour continuer...");
        Console.ReadLine();
    }


    private void WriteErrorMessage(string message) {
        Console.ForegroundColor = ConsoleColor.Red;
        Console.WriteLine(message);
        Console.ResetColor();
    }


    private void WriteTransactionSuccessMessage(Account account, Transaction? transaction) {
        if (transaction is null) {
            return;
        }
        Console.ForegroundColor = ConsoleColor.Green;
        var action = transaction.Type == Transaction_Type.DEPOSIT ? "déposé" : "déduit";
        if (account.Type == Account_Type.CREDIT) {
            Console.WriteLine($"Transaction {transaction.Type} effectué. Montant {action}: {transaction.Amount}$.\n Solde restant: {account.Limit - account.Amount}$");
        } else {
            Console.WriteLine($"Transaction {transaction.Type} effectué. Montant {action}: {transaction.Amount}$.\n Solde restant: {account.Amount}$");
        }
        Console.ResetColor();
        Console.Write("Appuyer sur une touche pour continuer.");
        Console.ReadLine();
    }


    private void WriteCreateSuccessMessage(string createType){
        Console.ForegroundColor = ConsoleColor.Green;
        Console.WriteLine($"{createType} créer avec succès!");
        Console.ResetColor();
    }


    private Transaction? CreateTransaction(Account SourceAccount, double amount, Transaction_Type transaction_type, Account? destinationAccount = null, string? ExternalAccountName = "") {
        if (amount == 0) {
            this.WriteWarning("Aucune transaction n'a été créer puisque le montant est de 0$");
            return null;
        }
        var transaction = new Transaction{Amount = amount, Type = transaction_type, SourceId = SourceAccount.Id, DestinationId = destinationAccount?.Id };
        if (!string.IsNullOrEmpty(ExternalAccountName)) {
            transaction.ExternalBillId = Guid.NewGuid();
            transaction.ExternalBillName = ExternalAccountName;
            transaction.isExternalBill = true;
        }
        return this._transactionController.Insert(transaction);
    }


    private double CalculateCreditWithdrawal(double amount, Account currentAccount, Transaction_Type transaction_type = Transaction_Type.WHITDRAWAL) {
        if (amount > currentAccount.Amount) {
            var creditAccount = this._accountController.GetCreditAccount(this._client);
            if (creditAccount is null) {
                throw new Exception("Action impossible. Le montant retiré est plus grand que le solde du compte.");
            }
            var creditAmount = amount - currentAccount.Amount;
            var availableCredit = creditAccount.Limit - creditAccount.Amount;
            if (amount > (currentAccount.Amount + availableCredit)) {
                throw new Exception("Action impossible. Le montant retiré est plus grand que le solde du compte.");
            }

            creditAccount.Amount += creditAmount;
            amount -= creditAmount;
            this._accountController.Update(creditAccount);
            this.WriteWarning($"Un montant de {creditAmount}$ a été déduit de votre compte de crédit pour compenser le manque de fond.");
            var creditTransaction = this.CreateTransaction(creditAccount, creditAmount, transaction_type);
            this.WriteTransactionSuccessMessage(creditAccount, creditTransaction);
        }
        return amount;
    }


    private double GetAmount(Account currentAccount){
        double amount = -1;

      while (amount % 10 != 0) {
            amount = Math.Round(Sharprompt.Prompt.Input<double>($"Combien voulez-vous retirer dans le compte {currentAccount.Type}? (Solde: {currentAccount.Amount}$)"), 2);
            if (amount % 10 != 0) {
                this.WriteErrorMessage(new Exception("Le montant doit être un multiple de 10."));
            }
            if (amount > 1000 || amount > this._bank.Amount) {
                var whitdrawLimit = amount > this._bank.Amount ? this._bank.Amount : 1000;
                amount = -1;
                this.WriteErrorMessage(new Exception($"Le montant retiré est supérieur à la limite de retrait de {whitdrawLimit}$."));
            }
        }
        return amount;
    }


    private User GetUser(List<User> users) {
        var userList = this.GetStringifiedUserList(users);
        Console.WriteLine("Veuillez sélectionner l'utilisateur pour lequel vous souhaité ajouté un compte");
        var selectedUser = Sharprompt.Prompt.Select<string>(this.FormatUserHeader(), userList);

        var currentUser = this.GetSelectedUser(selectedUser);
        return currentUser;
    }


    private Account GetAccount(List<Account> accounts) {
        var accountList = this.GetStringifiedAccountList(accounts);
        Console.WriteLine("Veuillez sélectionner le compte dans lequel vous souhaitez retirer");
        var selectedAccount = Sharprompt.Prompt.Select<string>(this.FormatAccountHeader(), accountList);
        
        var currentAccount = this.GetSelectedAccount(selectedAccount, accounts);
        return currentAccount;
    }



    private Account GetMortgageAccount(List<Account> accounts) {
        if (accounts.Count <= 0) {
            throw new Exception("Aucun compte hypotéchaire n'a été trouvé");
        }
        var accountList = this.GetStringifiedAccountList(accounts);
        Console.WriteLine("Veuillez sélectionner le compte que vous souhaitez collecter");
        var selectedAccount = Sharprompt.Prompt.Select<string>(this.FormatAccountHeader(), accountList);
        
        var currentAccount = this.GetSelectedAccount(selectedAccount, accounts);
        return currentAccount;
    }


    private List<string> GetStringifiedAccountList(List<Account> accounts, bool filter = false) {
        var accountList = new List<string>();
        foreach(Account account in accounts) {
            if (filter) {
                if (account.Type == Account_Type.CREDIT || account.Type == Account_Type.MORTGAGE) {
                    continue;
                }
            }
            accountList.Add(this.FormatAccount(account));
        }
        return accountList;
    }


    private List<string> GetStringifiedUserList(List<User> users) {
        var userList = new List<string>();
        foreach(User user in users) {
            userList.Add(this.FormatUser(user));
        }
        return userList;
    }


    private string FormatTransactionHeader() {
        var header = String.Format("| {0,-25} |", "Nom Client:");
            header += String.Format(" {0,-36} |", "Id");
            header += String.Format(" {0,-7} |", "amount");
            header += String.Format(" {0,-10} |", "Type");
            header += String.Format(" {0,-36} |", "SourceId");
            header += String.Format(" {0,-36} |", "External Bill Id");
            header += String.Format(" {0,-22} |", "External Bill Provider");
            header += String.Format(" {0,-36} |", "DestinationId");
        return header;
    }


    private string FormatTransaction(Transaction transaction) {
        var response = String.Format("  {0,-25} |", transaction!.SourceAccount!.User!.FullName);
            response += String.Format(" {0,-36} |", transaction.Id);
            response += String.Format(" {0,-7} |", $"{transaction.Amount}$");
            response += String.Format(" {0,-10} |", transaction.Type);
            response += String.Format(" {0,-36} |", transaction.SourceId);
            response += String.Format(" {0,-36} |", transaction.ExternalBillId);
            response += String.Format(" {0,-22} |", transaction.ExternalBillName);
            response += String.Format(" {0,-36} |", transaction.DestinationId);
        return response;
    }


    private string FormatUserHeader() {
        var header = String.Format("| {0,-37} |", "Id");
            header += String.Format(" {0,-20} |", "FirstName");
            header += String.Format(" {0,-20} |", "LastName");
            header += String.Format(" {0,-20} |", "Email");
            header += String.Format(" {0,-11} |", "PhoneNumber");
            header += String.Format(" {0,-12} |", "Role");
        return header;
    }


    private string FormatUser(User user) {
        var response = String.Format("| {0,-37} |", user.Id);
            response += String.Format(" {0,-20} |", user.FirstName);
            response += String.Format(" {0,-20} |", user.LastName);
            response += String.Format(" {0,-20} |", user.Email);
            response += String.Format(" {0,-11} |", user.PhoneNumber);
            response += String.Format(" {0,-12} |", user.Role.ToString());
        return response;
   }


    
    private string FormatAccountHeader() {
        var header = String.Format("| {0,-37} |", "# Compte");
            header += String.Format(" {0,-8} |", "Type");
            header += String.Format(" {0,-7} |", "Solde");
            header += String.Format(" {0,-18} |", "Montant Disponible"); 
            header += String.Format(" {0,-7} |", "Limite");
            header += String.Format(" {0,-7}", "Taux Intérêt (%)");
        return header;
    }


    private string FormatAccount(Account account) {
        var interest = "N/A";
        var limit = "N/A";
        var availableAmount = $"{account.Amount}$";
        if (account.Type == Account_Type.CREDIT) {
            availableAmount = $"{(account.Limit - account.Amount)}$";   
            interest = $"{account.Interest}%";
            limit = $"{account.Limit}$";
        }
        var stringifiedAccount = String.Format("| {0,-37} |", account.Id);
            stringifiedAccount += String.Format(" {0,-8} |", account.Type);
            stringifiedAccount += String.Format(" {0,-7} |", $"{account.Amount}$");
            stringifiedAccount += String.Format(" {0,-18} |", availableAmount); 
            stringifiedAccount += String.Format(" {0,-7} |", limit);
            stringifiedAccount += String.Format(" {0,-7}", interest);
        return stringifiedAccount;
    }


    private string FormatMortgageAccountHeader() {
        var header = String.Format("| {0,-37} |", "# Compte");
            header = string.Format("| {0,-25} |", "Nom du client");
            header += String.Format(" {0,-7} |", "Solde");
        return header;
    }


    private string FormatMortgageAccount(Account account) {
        var response = String.Format("| {0,-37} |", account.Id);
            response = string.Format("| {0,-25} |", account.User!.Id);
            response += String.Format(" {0,-7} |", $"{account.Amount}$");
        return response;
    }


    private string FormatAccountInterestHeader() {
        var header = String.Format("| {0,-37} |", "Id");
            header += String.Format(" {0,-8} |", "Type");
            header += String.Format(" {0,-7} |", "Solde");
            header += String.Format(" {0,-12} |", "Intérêt payé");
            header += String.Format(" {0,-7}", "Nouveau solde");
        return header;
    }


    private void DisplayAccountInterestAction(Account account, double fee, Account_Type account_type) {
        string updateMessage;
        var stringifiedAccount = String.Format("| {0,-37} |", account.Id);
            stringifiedAccount += String.Format(" {0,-8} |", account.Type);
            stringifiedAccount += String.Format(" {0,-7} |", $"{account.Amount}$");
        Console.ForegroundColor = ConsoleColor.Yellow;
        Console.Write(stringifiedAccount);
        Console.ForegroundColor = ConsoleColor.Green;
        if (account_type == Account_Type.CREDIT) {
            updateMessage = String.Format(" {0,-12} |", $"-{fee}$");
            updateMessage += String.Format(" {0,-7} |", $"{account.Amount - fee}$");
        } else {
            updateMessage = String.Format(" {0,-12} |", $"{fee}$");
            updateMessage += String.Format(" {0,-7} |", $"{account.Amount + fee}$");
        }

        Console.WriteLine(updateMessage);
        Console.ResetColor()                                ;
    }

#endregion
}
