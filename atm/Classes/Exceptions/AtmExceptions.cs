namespace atm.Classes.Exceptions;

public class LoginFailedException : System.Exception {
    public LoginFailedException() : base("Mauvais courriel ou mot de passe entré. Veuillez réessayé.") {}

}