using System.Collections.Generic;
using atm.Data;
using atm.Models;

namespace atm.Controllers;

public class AccountController(ApplicationDbContext context) {
    private readonly ApplicationDbContext _context = context;

#region "CRUD operations"

    public Account? GetCreditAccount(atm.Models.User model) {
        return _context.Accounts!.Where(x => x.UserId == model.Id && x.Type == Account_Type.CREDIT ).FirstOrDefault();
    }


    public List<Account>? GetAccountsBy(Account_Type accountType) {
        var accounts = _context.Accounts!.Where(x => x.Type == accountType).ToList();
        if (accounts.Count <= 0) {
            return null;
        }
        foreach(Account account in accounts) {
            account.User = _context.Users!.Where(x => x.Id == account.UserId).FirstOrDefault();
        }
        return accounts;
    }


    public List<Account> GetDepositableAccounts(atm.Models.User model) {
        // var accountTypeList = new List() { Account_Type.CHEQUING, Account_Type.SAVING };
        return _context.Accounts!.Where( x => x.UserId == model.Id && x.Type != Account_Type.CREDIT ).ToList();
    }


    public Account Update(atm.Models.Account model) {
        // Console.WriteLine(_context.Accounts!.GetAll().ToList().Count);
        this._context.Accounts!.Update(model);
        return model;
    }


    public void Insert(atm.Models.Account model) {
        if (model.Type != Account_Type.CHEQUING) {
            Console.WriteLine("uh");
            var ChequingAccount = _context.Accounts!.Where(x => x.UserId == model.UserId && x.Type == Account_Type.CHEQUING).FirstOrDefault();

            if (ChequingAccount is null) {
                throw new Exception("Le client n'a pas de compte Chèque.");
            }
        }
        
        if (model.Type == Account_Type.CREDIT) {
            var CreditAccount = _context.Accounts!.Where(x => x.UserId == model.UserId).First();
            if (CreditAccount is not null) {
                throw new Exception("Le client à déjà un compte de crédit.");
            }
            model.Interest = 5;
        } else if (model.Type == Account_Type.SAVING) {
            model.Interest = 1;
        }

        _context.Accounts!.Add(model);
        _context.SaveChanges();
        #if DEBUG
        this.Debug(model);
        #endif
    }


    public void CollectInterest(Account_Type account_type) {
        var accounts = _context.Accounts!.Where(x => x.Type == account_type).ToList();
        foreach(Account account in accounts) {
            account.Amount *= 1 + Math.Round(account.Interest / 100, 2);
        }
        _context.UpdateRange(accounts);
        _context.SaveChanges();
    }

#if DEBUG
    private void Debug(Account model) {
        var header = String.Format("| {0,-7} |", "Compte:");
                header += String.Format(" {0,-36} |", "Id");
                header += String.Format(" {0,-36} |", "Account");
                header += String.Format(" {0,-8} |", "Type");
                header += String.Format(" {0,-20} |", "Limit");
        var response = String.Format("  {0,-7} |", " ");
            response += String.Format(" {0,-36} |", model.Id);
            response += String.Format(" {0,-36} |", model.UserId);
            response += String.Format(" {0,-8} |", model.Type);
            response += String.Format(" {0,-20} |", model.Limit);
        Console.ForegroundColor = ConsoleColor.Yellow;
        Console.WriteLine("     DEBUG:");
        Console.WriteLine(header);
        Console.WriteLine(response);
        Console.ResetColor();
    }
#endif

#endregion

}