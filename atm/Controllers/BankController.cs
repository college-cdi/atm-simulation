using System.Collections.Generic;
using atm.Data;
using atm.Models;

namespace atm.Controllers;

public class BankController(ApplicationDbContext context) {
    private readonly ApplicationDbContext _context = context;

#region "CRUD operations"

    public Bank Get() {
        var bank = _context.Bank!.FirstOrDefault();
        if (bank is null) {
            throw new Exception("Aucune banque n'a été initialisé!");
        }
        return bank;
    }


    public Bank? Update(atm.Models.Bank model) {
        this._context.Bank!.Update(model);
        this._context.SaveChanges();
        Console.WriteLine(model.Amount);
        return model;
    }



    public void Insert(atm.Models.Account model) {
        if (model.Type == 0) {
            return;
        }

        if (model.Type != Account_Type.CHEQUING) {
            var ChequingAccount = _context.Accounts!.Where(x => x.UserId == model.UserId && x.Type == Account_Type.CHEQUING).FirstOrDefault();

            if (ChequingAccount is null) {
                throw new Exception("Le client n'a pas de compte Chèque.");
            }
        }
        
        if (model.Type == Account_Type.CREDIT) {
            var CreditAccount = _context.Accounts!.Where(x => x.UserId == model.UserId).First();
            if (CreditAccount is not null) {
                throw new Exception("Le client à déjà un compte de crédit.");
            }
        }
        model.Interest = 5;                                 
        _context.Accounts!.Add(model);

        _context.SaveChanges();
    }


    public void CollectInterest(Account_Type account_type) {
        var accounts = _context.Accounts!.Where(x => x.Type == account_type).ToList();
        foreach(Account account in accounts) {
            account.Amount *= 1 + Math.Round(account.Interest / 100, 2);
        }
        _context.UpdateRange(accounts);
        _context.SaveChanges();
    }

#endregion

}