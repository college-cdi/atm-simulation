using System.ComponentModel;
using System.Data;
using System.Runtime.InteropServices;
using System.Security;
using atm.Data;
using atm.Authenticate;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System.Reflection;
using atm.Models;
using atm.Models.Form;

namespace atm.Controllers;

/// <Summary>
/// Classe <c>Client</c> pour la gestion des opérations CRUD du client dans la DB.
/// <example>
/// test: 
/// <code> x = x+1 </code>
/// </example>
/// </Summary>
public class ClientController(ApplicationDbContext context) {
    private readonly ApplicationDbContext _context = context;

#region "Auth logic"

    /// <Summary>
    /// Méthode pour authentifier l'utilisateur.
    /// </Summary>
    /// <exception cref="atm.Classes.Exceptions.UserExceptions">
    /// Thrown when
    /// </exception>
    public User Login(LoginForm model) {
        if (string.IsNullOrEmpty(model.Nip)) {
            throw new atm.Classes.Exceptions.LoginFailedException();
        }

        var user = _context.Users?.Where(x => x.Email == model.Email).FirstOrDefault();
        if (user is null) {
            throw new atm.Classes.Exceptions.LoginFailedException();
        } else if (user.IsLocked) {
            throw new Exception("Compte bloqué. Veuillez contacter la banque pour débloquer votre compte.");
        }
        
        user.isLoggedIn = Authenticate.Authenticate.VerifyPassword(model.Nip, user.Nip, user.Hash);

        if (!user.isLoggedIn) {
            throw new atm.Classes.Exceptions.LoginFailedException();
        }
        
        // fetches user accounts
        user.Accounts = _context.Accounts?.Where(accounts => accounts.UserId == user.Id)?.ToList();
        return user;
    }


    public void Logout(User? model) {
        if (model is null) return;
        model.isLoggedIn = false;
        model = null;
    }

#endregion


#region "DB CRUD operations"

        internal List<User> GetAll() {
            var users = _context.Users!.ToList();
            return users;
        }


        internal User? GetBy(Guid id) {
            var user = _context.Users!.Find(id);
            return user;
        }


        /// <Summary>
        ///
        /// </Summary>
        internal User Insert(atm.Models.User model) {
            var user = _context.Users!.Where(x => x.Email == model.Email).FirstOrDefault();
            if (user is not null) {
                throw new Exception("Erreur: Un compte utilisant ce email existe déjà.");
            }

            model.Nip = Authenticate.Authenticate.Encrypt(model.Nip, out byte[] saltKey)!;
            model.Hash = saltKey;
            Console.WriteLine(model.Role);

            _context.Add(model);
            _context.SaveChanges();

#if DEBUG
            this.Debug(model);
#endif
            return model;
        }


        internal void Update(User model) {
            this._context.Users!.Update(model);
#if DEBUG
            this.Debug(model);
#endif
        }


#if DEBUG
        private void Debug(atm.Models.User model) {
            var header = String.Format("| {0,-5} |", "User:");
                header += String.Format(" {0,-36} |", "Id");
                header += String.Format(" {0,-20} |", "FirstName");
                header += String.Format(" {0,-20} |", "LastName");
                header += String.Format(" {0,-20} |", "Email");
                header += String.Format(" {0,-7} |", "Bloqué?");
                header += String.Format(" {0,-11} |", "PhoneNumber");
                header += String.Format(" {0,-12} |", "Role");
            var response = String.Format("  {0,-5} |", " ");
                response += String.Format(" {0,-36} |", model.Id);
                response += String.Format(" {0,-20} |", model.FirstName);
                response += String.Format(" {0,-20} |", model.LastName);
                response += String.Format(" {0,-20} |", model.Email);
                response += String.Format(" {0,-7} |", model.IsLocked);
                response += String.Format(" {0,-11} |", model.PhoneNumber);
                response += String.Format(" {0,-12} |", model.Role.ToString());
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("     DEBUG:");
            Console.WriteLine(header);
            Console.WriteLine(response);
            Console.ResetColor();
        }
#endif

#endregion

}