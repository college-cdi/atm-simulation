using atm.Data;
using atm.Models;


class TransactionController(ApplicationDbContext context) {
    private readonly ApplicationDbContext _context = context;

    public List<Transaction> GetAll() {
        var transactions = this._context.Transactions!.ToList();
        foreach (Transaction transaction in transactions) {
            transaction.SourceAccount = this._context.Accounts!.Where(x => x.Id == transaction.SourceId)?.FirstOrDefault();
            transaction.SourceAccount!.User = this._context.Users!.Where(user => user.Id == transaction.SourceAccount!.UserId)?.FirstOrDefault();
        }
        return transactions;
    }

    public Transaction Insert(atm.Models.Transaction model) {
        this._context.Transactions!.Add(model);
        this._context.SaveChanges();
#if DEBUG
        this.Debug(model); // TODO: delete ? 
#endif
        return model;
    }


#if DEBUG
    private void Debug(atm.Models.Transaction model) {
        var header = String.Format("| {0,13} |", "Transaction :");
            header += String.Format(" {0,-36} |", "Id");
            header += String.Format(" {0,-7} |", "amount");
            header += String.Format(" {0,-10} |", "Type");
            header += String.Format(" {0,-36} |", "SourceId");
        if (model.isExternalBill) {
            header += String.Format(" {0,-36} |", "External Bill Id");
            header += String.Format(" {0,-22} |", "External Bill Provider");
        } else {
            header += String.Format(" {0,-36} |", "DestinationId");
        }
        var response = String.Format("  {0,-13} |", " ");
            response += String.Format(" {0,-36} |", model.Id);
            response += String.Format(" {0,-7} |", $"{model.Amount}$");
            response += String.Format(" {0,-10} |", model.Type);
            response += String.Format(" {0,-36} |", model.SourceId);
        if (model.isExternalBill) {
            response += String.Format(" {0,-36} |", model.ExternalBillId);
            response += String.Format(" {0,-22} |", model.ExternalBillName);
        } else {
            response += String.Format(" {0,-36} |", model.DestinationId);
        }
        Console.ForegroundColor = ConsoleColor.Yellow;
        Console.WriteLine("     DEBUG:");
        Console.WriteLine(header);
        Console.WriteLine(response);
        Console.ResetColor();
    }
#endif

}