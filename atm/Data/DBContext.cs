using Microsoft.EntityFrameworkCore;
using atm.Models;
using Microsoft.Extensions.DependencyInjection;
using System.Diagnostics;
using System.Collections.Generic;
using System;

namespace atm.Data;

public class ApplicationDbContext : DbContext {
    
    public ApplicationDbContext() {}

    public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options) {
    }


        protected override void OnModelCreating(ModelBuilder builder)
    {
        base.OnModelCreating(builder);
    }

    public virtual void Seed() {
        var users = new List<User> {
            new() { Id = Guid.NewGuid() ,FirstName = "Normal", LastName = "User", PhoneNumber = "5141231122", Nip = "1234", Email = "Test@email.com"},
            new() { Id = Guid.NewGuid(), FirstName = "Admin", LastName = "User",  Role = Roles.Admin, PhoneNumber = "5142224545", Nip = "0000", Email = "admin@email.com"}
            };
        var accounts = new List<Account> {
            new() { Id = Guid.NewGuid(), UserId = users[0].Id, Type = Account_Type.CHEQUING, Interest = 0, Amount = 0 },
            new() { Id = Guid.NewGuid(), UserId = users[0].Id, Type = Account_Type.SAVING, Interest = 1, Amount = 2000},
            new() { Id = Guid.NewGuid(), UserId = users[0].Id, Type = Account_Type.CREDIT, Interest = 5, Amount = 200, Limit = 300 },
            new() { Id = Guid.NewGuid(), UserId = users[0].Id, Type = Account_Type.MORTGAGE, Interest = 0, Amount = 20000 },
        };

        var transactions = new List<Transaction> {
            new() { Id = Guid.NewGuid(), Amount = 30, Type = Transaction_Type.WHITDRAWAL, SourceId = accounts[0].Id },
            new() { Id = Guid.NewGuid(), Amount = 200.90, Type = Transaction_Type.DEPOSIT,  SourceId = accounts[0].Id },
            new() { Id = Guid.NewGuid(), Amount = 180, Type = Transaction_Type.DEPOSIT,  SourceId = accounts[0].Id },
            new() { Id = Guid.NewGuid(), Amount = 20, Type = Transaction_Type.BILLING,  SourceId = accounts[0].Id, ExternalBillId = Guid.NewGuid(), ExternalBillName = "Bank"},
            new() { Id = Guid.NewGuid(), Amount = 2091.90, Type = Transaction_Type.DEPOSIT,  SourceId = accounts[0].Id },
        };

        foreach (User user in users) {
            user.Nip = Authenticate.Authenticate.Encrypt(user.Nip, out byte[] saltKey)!;
            user.Hash = saltKey;
        }

        this.Users!.AddRange(users);
        this.Accounts!.AddRange(accounts);
        this.Transactions!.AddRange(transactions);
        this.Bank!.Add(new Bank{Amount = 2000});
        this.SaveChanges();
    }

    public DbSet<User>? Users { get; set; }
    public DbSet<Account>? Accounts { get; set; }
    public DbSet<Transaction>? Transactions { get; set; }
    public DbSet<Bank>? Bank { get; set; }
    // public DbSet<Transaction_Type> Transaction_Types { get; set; }
    // public DbSet<Account_Type> Account_Types { get; set; }
}
