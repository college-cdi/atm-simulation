using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace atm.Models;

public enum Account_Type {
    CHEQUING,
    SAVING,
    CREDIT,
    MORTGAGE,
}

public class Account {
    [Key]
    public Guid Id {get; set;}
    [ForeignKey("UserId")]
    public Guid UserId {get; set;} = default!;
    public Account_Type Type {get; set;} = default!;
    public double Interest {get; set;} = 0;
    public double Amount {get; set;} = 0;
    public double Limit {get; set;} = 0;
    public DateTime CreatedAt {get; set;} = DateTime.Now;

    public virtual User? User {get; set;}

}
