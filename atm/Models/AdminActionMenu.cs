using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;

namespace atm.Models;

enum AdminActionMenu {
    [Display(Name = "Créer un client")]
    CreateClient,
    [Display(Name = "Créer un compte client")]
    CreateAccount, // n'importe quel type
    [Display(Name = "Afficher les transactions d'un compte")]
    DisplayAccountTransaction,
    [Display(Name = "Bloqué ou Débloqué l'accés d'un client")]
    ToggleLockClientAccount,
    [Display(Name = "Ajouter de l'argent dans le guichet")]
    AddMoneyToATM, // limit 20 000$
    [Display(Name = "Fermer le guichet")]
    StopApp,
    [Display(Name = "Payer de l'intérêt aux comptes épargnes (1%)")]
    PayInterest,
    [Display(Name = "Prélever un montant d'un compte hypothécaire")] //  Prélever le montant de son choix à un compte hypothécaire spécifique. Si le compte n'a pas un solde suffisant, la différence sera prélevée de la marge de crédit du client si elle existe. Sinon, un message d'erreur sera affiché.
    CollectMortgage,
    [Display(Name = "Collecter l'intérêt des comptes de crédits (5%)")]
    CollectCreditInterest,
    [Display(Name = "Se Déconnecter")]
    Logout,
}