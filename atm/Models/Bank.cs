using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace atm.Models;


public class Bank {
    [Key]
    public Guid Id {get; set;}
    [ForeignKey("UserId")]
    public double Amount {get; set;} = 0;

}
