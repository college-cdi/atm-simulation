namespace atm.Models.Form;

public class LoginForm {
    public string? Email {get; set;}
    public string? Nip {get; set;}
}