namespace atm.Models;

public enum Transaction_Type {
    WHITDRAWAL,
    DEPOSIT,
    TRANSFER,
    BILLING,
    INTEREST,
}

public class Transaction {
    public Guid Id {get; set;}
    public double Amount {get; set;} = default!;
    public Transaction_Type Type {get; set;} = default!; 
    public Guid SourceId {get; set;} = default!;
    public Guid? DestinationId {get; set;}
    public Guid? ExternalBillId {get; set;}
    public bool isExternalBill {get; set;} = false;
    public string? ExternalBillName {get; set;}
    public DateTime CreatedAt {get; set;} = DateTime.Now;


    public virtual Account? SourceAccount { get; set; }
    public virtual Account? DestinationAccount { get; set; }
}