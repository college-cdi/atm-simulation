using System.ComponentModel.DataAnnotations;

namespace atm.Models;

public class User {
    public Guid Id {get; set;} = Guid.NewGuid();
    public string FirstName {get; set;} = default!;
    public string LastName {get; set;} = default!;
    public string Email {get; set;} = default!;
    public string PhoneNumber {get; set;} = default!;
    public Roles Role {get; set;} = Roles.User  ;
    public byte[] Hash {get; set;} = default!;
    public bool IsLocked {get; set;} = false;
    public DateTime CreatedAt {get; set;} = DateTime.Now;


    public virtual List<Account>? Accounts {get; set;}

    [MinLength(4), MaxLength(6)]
    [Required(ErrorMessage = "Le nip doit contenir entre 4 et 6 chiffres")]
    public virtual string Nip {get; set;} = default!;
    public virtual bool isLoggedIn {get; set;} = false;

    public virtual string FullName {get {
            return $"{FirstName} {LastName}";
        }
    }

}

public enum Roles {
    [Display(Name = "Utilisateur")]
    User,
    [Display(Name = "Administrateur")]
    Admin
}