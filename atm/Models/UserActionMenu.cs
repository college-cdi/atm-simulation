using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;

namespace atm.Models;

enum UserActionMenu {
    [Display(Name = "Effectuer un dépot")]
    Deposit,
    [Display(Name = "Effectuer un retrait")]
    Withdrawal,
    [Display(Name = "Effectuer un Transfert")]
    Transfert,
    [Display(Name ="Payer une facture")]
    BillPayment,
    [Display(Name = "Consulter mes comptes")]
    ConsultAccounts,
    [Display(Name = "Se déconnecter")]
    Logout
}