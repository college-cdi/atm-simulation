﻿using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using atm.Classes;
using atm.Data;
using atm.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.DependencyInjection;

namespace atm;

class Program {

    static void Main(string[] args) {
        var dbContext = InitializeContext();
        Application application = Application.New(dbContext);
        application.Run();
    }


    public static ApplicationDbContext InitializeContext() {
        var optionsBuilder = new DbContextOptionsBuilder<ApplicationDbContext>();
        string connectionString = "database";
        optionsBuilder.UseInMemoryDatabase(connectionString);
        ApplicationDbContext _context = new(optionsBuilder.Options);
        Authenticate.Authenticate.New(_context);
        if (_context.Users?.FirstOrDefault() == null) {
            _context.Seed();
        }

        return _context;
    }

}
