using System.Globalization;
using System.Security.Cryptography;
using System.Text;
using atm.Data;
using Microsoft.VisualBasic;
using Npgsql.EntityFrameworkCore.PostgreSQL.Query.Internal;

namespace atm.Authenticate;

/// <Summary>
///
/// </Summary>
public static class Authenticate {
    private static Authenticator? auth;

    public static void New(ApplicationDbContext context) {
        if (Authenticate.auth is null) {
            Authenticate.auth = new Authenticator(context);
        }
    }

    public static bool VerifyPassword(string password, string hash, byte[] salt) {
        if (Authenticate.auth is null) throw new Exception("Authenticate has not yet been initialized!");
        return Authenticate.auth.VerifyPassword(password, hash, salt);
    }


    public static string? Encrypt(string password, out byte[] salt) {
        if (Authenticate.auth is null) throw new Exception("Authenticate has not yet been initialized!");
        return Authenticate.auth.Encrypt(password, out salt);
    }


    private class Authenticator(ApplicationDbContext context) {
        private readonly ApplicationDbContext _context = context;
        private static HashAlgorithmName hashAlgorithm = HashAlgorithmName.SHA512;
        private const int keySize = 64;
        private const int iterations = 350000;

        internal string Encrypt(string password, out byte[] salt) {
            salt = RandomNumberGenerator.GetBytes(keySize);
            var hash = Rfc2898DeriveBytes.Pbkdf2(
                Encoding.UTF8.GetBytes(password),
                salt,
                iterations,
                hashAlgorithm,
                keySize);
            return Convert.ToHexString(hash);
        }

        internal bool VerifyPassword(string password, string hash, byte[] salt) {
            var hashToCompare = Rfc2898DeriveBytes.Pbkdf2(password, salt, iterations, hashAlgorithm, keySize);
            return CryptographicOperations.FixedTimeEquals(hashToCompare, Convert.FromHexString(hash));
        }
    }
}